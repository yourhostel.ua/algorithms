from python.insertion_sort.insertion_sort import insertion_sort
from python.merge_sort.merge_sort import merge_sort


def main():
    # Сортування вставками
    arr = [5, 2, 4, 6, 1, 3, -1, 20, 0]
    insertion_sort(arr)
    print("Sorted with insertion sort:", arr)

    # Сортування злиттям
    array = [38, 27, 43, 3, 9, 82, 10, 0, -15]
    sorted_array = merge_sort(array)
    print("Sorted with merge sort:", sorted_array)


if __name__ == "__main__":
    main()
