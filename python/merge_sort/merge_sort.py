def merge_sort(array):
    # Умова виходу з рекурсії
    if len(array) <= 1:
        return array

    # Знаходження середини масиву для поділу на дві частини
    mid = len(array) // 2

    # Рекурсивно сортуємо ліву та праву частини
    left = merge_sort(array[:mid])
    right = merge_sort(array[mid:])

    # Злиття відсортованих частин
    return merge(left, right)


def merge(left, right):
    result = []
    i = j = 0

    # Поки не досягнуто кінця одного з масивів
    while i < len(left) and j < len(right):
        # Додаємо менший елемент до результуючого масиву
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    # Додаємо залишкові елементи лівого та правого масивів
    result.extend(left[i:])
    result.extend(right[j:])

    # Повертаємо результат злиття
    return result
