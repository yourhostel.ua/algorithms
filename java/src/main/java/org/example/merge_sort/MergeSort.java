package org.example.merge_sort;

public class MergeSort {
    // Головний метод для сортування масиву
    public static int[] mergeSort(int[] array) {
        // Починаємо рекурсивне сортування з повного масиву
        return mergeSort(array, 0, array.length - 1);
    }

    private static int[] mergeSort(int[] array, int l, int r) {
        // Умова виходу з рекурсії: якщо поділ досяг одного елемента
        if (l >= r) {
            // Повертаємо масив з одного елемента
            return new int[]{array[l]};
        }
        // Знаходження середини масиву для поділу на дві частини
        int m = l + r >>> 1; // int m = l + (r - l) / 2;
        // Рекурсивно сортуємо ліву частину
        int[] left = mergeSort(array, l, m);
        // Рекурсивно сортуємо праву частину
        int[] right = mergeSort(array, m + 1, r);

        // Злиття відсортованих частин
        return merge(left, right);
    }

    // Метод для злиття двох відсортованих масивів
    private static int[] merge(int[] xs, int[] ys) {
        int[] zs = new int[xs.length + ys.length];
        int i = 0; // xs Індекс для лівого масиву
        int j = 0; // ys Індекс для правого масиву
        int k = 0; // zs Індекс для результуючого масиву

        // Поки не досягнуто кінця одного з масивів
        while (i < xs.length && j < ys.length) {
            // Додаємо менший елемент до результуючого масиву
            zs[k++] = xs[i] < ys[j] ? xs[i++] : ys[j++];
        }

        // Додаємо залишкові елементи лівого масиву
        while (i < xs.length) {
            zs[k++] = xs[i++];
        }
        // Додаємо залишкові елементи правого масиву
        while (j < ys.length) {
            zs[k++] = ys[j++];
        }
        // Повертаємо результат злиття
        return zs;
    }

}
