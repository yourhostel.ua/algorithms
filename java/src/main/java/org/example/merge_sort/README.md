# Процес сортування злиттям

Сортування злиттям для масиву `{38, 27, 43, 3, 9, 82, 10, 0, -15}` проходить в кілька етапів:

## Розбиття на підмасиви

| Етап | Масиви |
|------|--------|
| 1    | `[38, 27, 43, 3, 9, 82, 10, 0, -15]` |
| 2    | `[38, 27, 43, 3, 9]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[82, 10, 0, -15]` |
| 3    | `[38, 27]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[43, 3, 9]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[82, 10]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[0, -15]` |
| 4    | `[38]` `[27]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[43]` `[3, 9]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[82]` `[10]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[0]` `[-15]` |
| 5    | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[3]` `[9]` |

## Злиття у відсортований порядок

| Етап | Масиви |
|------|--------|
| 1    | `[27, 38]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[3, 9, 43]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[10, 82]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[-15, 0]` |
| 2    | `[3, 9, 27, 38, 43]` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `[-15, 0, 10, 82]` |
| 3    | `[-15, 0, 3, 9, 10, 27, 38, 43, 82]` |

## Відсортований масив

`[-15, 0, 3, 9, 10, 27, 38, 43, 82]`

Процес сортування злиттям демонструє використання стратегії "розділяй та володарюй", дозволяючи сортувати масиви великого розміру.
