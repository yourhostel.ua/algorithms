package org.example;
import java.util.Arrays;

import static org.example.insertion_sort.java.InsertionSort.sort;
import static org.example.merge_sort.MergeSort.mergeSort;


public class App 
{
    public static void main( String[] args ){
        /**
         * Сортування вставками
         */
        int[] arr1 = {5, 2, 4, 6, 1, 3, -1, 20, 0};
        sort(arr1);
        System.out.printf("Sorted with insertion sort: %s\n", Arrays.toString(arr1));

        /**
         * Сортування злиттям
         */
        int[] arr2 = {38, 27, 43, 3, 9, 82, 10, 0, -15};
        int[] sortedArray = mergeSort(arr2);
        System.out.printf("Sorted with merge sort: %s\n", Arrays.toString(sortedArray));


    }
}
