package org.example.insertion_sort.java;
#

public class InsertionSort {
    public static void sort(int arr[]) {
        int n = arr.length;
        for (int i=1; i<n; ++i) {
            int key = arr[i];
            int j = i-1;
            /* Перемістити елементи arr[0..i-1], тобто
                більше ключа, на одну позицію вперед
                свого поточного становища */
            while (j>=0 && arr[j] > key) {
                arr[j+1] = arr[j];
                j = j-1;
            }
            arr[j+1] = key;
        }
    }

}
